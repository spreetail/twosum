/**

Given an array of numbers arr, return all 2 element combinations of numbers that sum to target n

Examples:

twoSum([1,1,4,4], 5) // [[1,4], [1,4], [1,4], [1,4]]

twoSum( [4,2,3,9,-4,8,3], 5) //   [[ 2, 3 ], [ 2, 3 ], [ 9, -4 ] ]

twoSum( [2,9,2,2,2,2,2], 5) // []

 */

const twoSum = (arr, target) => {
    let results = [];

    return results;
}

const testCases = [
    [[1,1,4,4], 5, [[1,4]]],
    [[-1, 5, 4, 7, 4, 2, 1, 5, 3, -4, 9], 5, [[4,1], [2,3], [-4,9]]],
    [[-1, 5, 2, 1, 5, 3, -4, 9], 5, [[2,3], [-4,9]]],
    [[1,1,2,2,3,3,4,4], 4, [[1,3],[2,2]]],
  ]
   
  testCases.forEach(e => {
    const result = JSON.stringify(twoSum(e[0], e[1]))
    const expected = JSON.stringify(e[2]);
    if (result != expected) {
      console.log("Failed. Result was " + result + " but expected was " + expected)
    } else {
      console.log("OK")
    }
})
